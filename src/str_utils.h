/*
Star SP300 Driver for Linux
Copyright (C) 2021  Jindřich Veselý

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#pragma once

#ifndef STR_UTILS_H
#define STR_UTILS_H

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

char* str_read();
char** str_split(char* in, const char c_delim, size_t* n_args);
int str_cut(char* str, int begin, int len);

#endif