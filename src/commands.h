/*
Star SP300 Driver for Linux
Copyright (C) 2021  Jindřich Veselý

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#ifndef COMMANDS_H
#define COMMANDS_H

#include "config.h"
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>

#ifndef USE_USB

//TODO: adresu definovat zde, to si vemete z BIOSu
#define BASE 0x378 // /dev/lp0
#define TRANSFER_RATE 1000
#define TRANFER_SLEEP 1000000 / TRANSFER_RATE

#else

#include <libusb-1.0/libusb.h>

//TODO: definovat zde, adresu si vemte z lsusb
#define VENDOR_ID 0x1a86
#define PRODUCT_ID 0x7584

extern struct libusb_device_handle* device_handle;
extern libusb_context* ctx;

#endif

//TODO: příkazy
// pro přesnější popis koukni do manuálu https://www.starmicronics.com/support/Mannualfolder/sp300pm.pdf
// kody jsou cca v pořadí, duplikáty jsou přeskočeny a je použit první kod který spouští funkci
// názvy jsou jen podobné
// kde je v příkazu FF tak se bere jako parameter(prm) do send_cmd
// pokud příkaz nemá parameter tak prm je 0
enum printer_command {

	//CHARACTER SETTING
	PRNT_SELECT_INTERN_CHAR_SET = 0x1B52FF,
	PRNT_SELECT_CHAR_SET_IBM1 = 0x1B36,
	PRNT_SELECT_CHAR_SET_IBM2 = 0x1B37,

	//Zapne vypisování textu s 2x šířkou
	PRNT_SELECT_EXPANDED_CHAR_MODE = 0x0E,
	PRNT_CANCEL_EXPANDED_CHAR_MODE = 0x14,

	//Text je tlustý
	PRNT_SELECT_EMPHASIZED_PRINT_MODE = 0x1B45,
	PRNT_CANCEL_EMPHASIZED_MODE = 0x1B46,

	//podthává text, ignoruje mezery udělané horizontal tabem
	PRNT_SELECT_UNDERLINE_MODE = 0x1B2D31,
	PRNT_CANCEL_UNDERLINE_MODE = 0x1B2D00,

	//nadtrhává text, ignoruje mezery udělané horizontal tabem
	PRNT_SELECT_UPPERLINE_MODE = 0x1B5F31,
	PRNT_CANCEL_UPPERLINE_MODE = 0x1B5F00,

	// prohodí barvy
	PRNT_SELECT_HIGHLIGHED_MODE = 0x1B34,
	PRNT_CANCEL_HIGHLIGHED_MODE = 0x1B35,

	// zapne psaní textu s 2x výškou
	PRNT_SELECT_VERTICAL_EXPADED_CHAR_MODE = 0x1B6831,
	PRNT_CANCEL_VERTICAL_EXPADED_CHAR_MODE = 0x1B6830,

	//LINE SPACING
	PRNT_LINE_FEED = 0x0A,
	PRNT_FEED_N_LINES = 0x1B61FF,

	//PAGE LAYOUT
	PRNT_PAGE_FEED = 0x0C,
	PRNT_SET_PAGE_LENGTH_LINES = 0x1B43FF,

	PRNT_SET_VERTICAL_TAB_POS = 0x1B42FFFF,
	PRNT_EXEC_VERTICAL_TAB = 0x0B,

	PRNT_SET_BOTTOM_MARGIN = 0x1B4EFF,
	PRNT_CANCEL_BOTTOM_MARGIN = 0x1B4F,

	PRNT_SET_LEFT_MARGIN = 0x1B6CFF,
	PRNT_SET_RIGHT_MARGIN = 0x1B51FF,

	PRNT_SET_HORIZONTAL_TAB_POS = 0x1B44FFFF,
	PRNT_EXEC_HORIZONTAL_TAB = 0x09,

	PRNT_DEFINE_X72_INCH_LINE_FEED = 0x1B41FF,
	PRNT_SET_X72_INCH_LINE_FEED = 0x1B32,

	PRNT_SET_1_6_INCH_LINE_FEED = 0x1B7A31,

	// GRAPHICS PRINTING
	// DOWNLOAD CHARACTERS
	// Jelikož moje API nepodporuje posílání těchto dat pomocí předefinovanýách příkazu tak si to budete muset napsat sami :D
	// stejně si musíte přečíst tu dokumentaci aby jste rozuměli tomu jak to funguje

	// Peripheral units
	// nevedem

	//Other
	PRNT_BUZZER = 0x1E,
	PRNT_CANCEL_AND_INIT = 0x18,
	PRNT_UNI_DIR_PRINT = 0x1B5531,
	PRNT_BI_DIR_PRINT = 0x1B5530,
	PRNT_INIT = 0x1B40,
	PRNT_CUT = 0x1B6430

};

void send_cmd(uint32_t o_cmd, uint16_t o_param);
void send_data(const void* buf, size_t size);
void flush_buf();
void clean_buf();

#endif