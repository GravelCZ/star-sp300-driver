/*
Star SP300 Driver for Linux
Copyright (C) 2021  Jindřich Veselý

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include <png.h>
#include <stdlib.h>
#include <stdio.h>
#include "image_parser.h"
#include "commands.h"

typedef struct {
    png_uint_32 width;
    png_uint_32 height;

    png_bytepp red_map;
    png_bytepp green_map;
    png_bytepp blue_map;
    png_bytepp alpha_map;

    png_byte color_type;
} Image;

typedef struct {
    png_byte red;
    png_byte green;
    png_byte blue;
} Color;

Image* alloc_image(png_uint_32 width, png_uint_32 height)
{
    Image* image = malloc(sizeof(Image));

    image->width = width;
    image->height = height;

    size_t amount = sizeof(png_bytep) * image->width;
    image->red_map = malloc(amount);
    image->green_map = malloc(amount);
    image->blue_map = malloc(amount);
    image->alpha_map = malloc(amount);

    int x;
    for (x = 0; x < image->width; x++)
    {
        amount = sizeof(png_bytep) * image->height;
        image->red_map[x] = malloc(amount);
        image->green_map[x] = malloc(amount);
        image->blue_map[x] = malloc(amount);
        image->alpha_map[x] = malloc(amount);
    }

    return image;
}

void free_image(Image* image)
{
    int x;
    for (x = 0; x < image->width; x++)
    {
        free(image->red_map[x]);
        free(image->green_map[x]);
        free(image->blue_map[x]);
        free(image->alpha_map[x]);
    }

    free(image->red_map);
    free(image->green_map);
    free(image->blue_map);
    free(image->alpha_map);

    free(image);
}

Image* read_image(char* filename)
{
    png_bytepp row_ptr;
    png_structp png_ptr;
    png_infop info_ptr;

    unsigned y, x;

    unsigned char header[8];

    FILE *fp = fopen(filename, "rb");
    if (!fp)
    {
        fprintf(stderr, "File %s could not be opened.", filename);
        return NULL;
    }

    fread(header, 1, 8, fp);
    if (png_sig_cmp(header, 0, 8))
    {
        fprintf(stderr, "File %s is not PNG", filename);
        return NULL;
    }

    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr)
    {
        fprintf(stderr, "Failed to create PNG struct");
        return NULL;
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
        fprintf(stderr, "Failed to create PNG info struct");
        return NULL;
    }

    png_init_io(png_ptr, fp);
    png_set_sig_bytes(png_ptr, 8);
    png_read_info(png_ptr, info_ptr);

    Image* image = alloc_image(png_get_image_width(png_ptr, info_ptr), png_get_image_height(png_ptr, info_ptr));

    image->color_type = png_get_color_type(png_ptr, info_ptr);
    if (image->color_type != PNG_COLOR_TYPE_RGB && image->color_type != PNG_COLOR_TYPE_RGBA)
    {
        fprintf(stderr, "Unsupported PNG format\n");
        return NULL;
    }

    if (png_get_bit_depth(png_ptr, info_ptr) == 16)
    {
        png_set_strip_16(png_ptr);
    }

    if (image->color_type == PNG_COLOR_TYPE_RGB)
    {
        png_set_filler(png_ptr, 0xFF, PNG_FILLER_AFTER);
    }

    png_read_update_info(png_ptr, info_ptr);

    //ano, tady má být height 2x
    row_ptr = (png_bytepp) malloc(sizeof(png_bytep) * image->height);
    for (y = 0; y < image->height; y++)
    {
        row_ptr[y] = (png_bytep) malloc(png_get_rowbytes(png_ptr, info_ptr));
    }

    png_read_image(png_ptr, row_ptr);
    fclose(fp);
    png_destroy_read_struct(&png_ptr, &info_ptr, NULL);

    for (y = 0; y < image->height; y++)
    {
        png_bytep row = row_ptr[y];
        for (x = 0; x < image->width; x++)
        {
            png_bytep ptr = &(row[x * 4]);

            image->red_map[x][y] = ptr[0];
            image->green_map[x][y] = ptr[1];
            image->blue_map[x][y] = ptr[2];
            image->alpha_map[x][y] = ptr[3];
        }
    }

    for (y = 0; y < image->height; y++)
    {
        free(row_ptr[y]);
    }
    free(row_ptr);

    return image;
}

Image* scale_image(Image* input_image)
{
    Image* image = alloc_image(200, 200);

    int x, y;

    for (x = 0; x < image->width; x++)
    {
        for (y = 0; y < image->height; y++)
        {
            size_t xp = x * input_image->width / image->width;
            size_t yp = y * input_image->height / image->height;

            image->red_map[x][y] = input_image->red_map[xp][yp];
            image->green_map[x][y] = input_image->green_map[xp][yp];
            image->blue_map[x][y] = input_image->blue_map[xp][yp];
            image->alpha_map[x][y] = input_image->alpha_map[xp][yp];
        }
    }

    //free_image(input_image);

    return image;
}

//ano, na to abych zjistil jestli pixel víc nežný než bílý tak je velice složité :DDD
double get_distance(Color* color1, Color* color2)
{
    double r = color1->red - color2->red;
    double g = color1->green - color2->green;
    double b = color1->blue - color2->blue;

    return (r * r) + (g * g) + (b * b);
}

uint8_t is_black(int x, int y, Image* input_image)
{
    Color colors[] = {
        {255,255,255},
        {0,0,0}
    };
    Color c = {
        input_image->red_map[x][y],
        input_image->green_map[x][y],
        input_image->blue_map[x][y]
    };

    int idx = 0;
    double best = -1;
    for (int i = 0; i < 2; i++)
    {
        double distance = get_distance(&colors[i], &c);
        if (distance < best || best == -1)
        {
            best = distance;
            idx = i;
        }
    }

    return idx;
}

void send_image_to_printer(Image* input_image)
{
    //definice na správný tisk obrázku
    send_cmd(PRNT_DEFINE_X72_INCH_LINE_FEED, 8);
    send_cmd(PRNT_SET_X72_INCH_LINE_FEED, 0);
    send_cmd(PRNT_CANCEL_BOTTOM_MARGIN, 0);
    send_cmd(PRNT_SET_PAGE_LENGTH_LINES, 35);
    send_cmd(PRNT_SET_LEFT_MARGIN, 0);
    send_cmd(PRNT_SET_RIGHT_MARGIN, 40);

    int line, x, y, y_pos, y_extra;
    for(line = 0; line < input_image->height / 8; line++)
    {
        y_pos = line * 8;
        // 1B4B = příkaz, za FF je dosazen argument 0xC8
        send_cmd(0x1B4BFF00, 0xC8);
        uint8_t* row_data = (uint8_t*) malloc(input_image->width);
        for (x = 0; x < input_image->width; x++)
        {
            uint8_t value = 0;
            for (y_extra = 0; y_extra < 8; y_extra++)
            {
                y = y_extra + y_pos;

                if (is_black(x ,y, input_image))
                {
                    value |= 1;
                }
                if (y_extra != 7)
                {
                    value <<= 1;
                }
            }
            row_data[x] = value;
        }
        send_data(row_data, 200);
        free(row_data);
        send_cmd(PRNT_LINE_FEED, 0);
    }
    send_cmd(PRNT_SET_1_6_INCH_LINE_FEED, 0);
    send_cmd(PRNT_PAGE_FEED, 0);
    send_cmd(PRNT_CUT, 0);
    send_cmd(PRNT_BUZZER, 0);
    flush_buf();
}

void print_image(char* filename)
{
    Image* img = read_image(filename);
    if (img == NULL)
    {
        return;
    }
    if (img->width != 200 && img->height != 200)
    {
        Image* img2 = scale_image(img);
        free_image(img);
        img = img2;
    }
    printf("Sending image to printer\n");
    send_image_to_printer(img);
    free_image(img);
}