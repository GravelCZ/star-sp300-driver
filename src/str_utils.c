/*
Star SP300 Driver for Linux
Copyright (C) 2021  Jindřich Veselý

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "str_utils.h"

char* str_read()
{
	size_t size = 20;
	char* string;
	int ch;
	size_t len = 0;

	string = malloc(sizeof(char) * size);

	while ((ch = getchar()) != EOF && ch != '\n')
	{
		string[len++] = ch;
		if (len == size)
		{
			size += 20;
			string = realloc(string, sizeof(char) * size);
		}
	}

	string[len++] = '\0';

	return realloc(string, sizeof(*string) * len);
}

char** str_split(char* in, const char c_delim, size_t* n_args)
{
	char** result;
	size_t count = 0;
	char* tmp = in;
	char* last_delim = 0;
	char delim[2];
	delim[0] = c_delim;
	delim[1] = 0;

	//kolikrát rozdělíme text
	// *tmp znamená dokud nenarazíme na NULL tady konek textu
	// protože NULL je 0 a cokoliv jineho je 1 (aka true a false)
	while (*tmp)
	{
		if (*tmp == c_delim)
		{
			count++;
			last_delim = tmp;
		}
		tmp++;
	}

	//přidá prostor pro koncový znak
	if (last_delim < (in + strlen(in) - 1))
	{
		count++;
	}
	//ano, toto pořadí takto mý být jelikož nepočítáme koncový null byte
	*n_args = count;
	count++;

	result = malloc(sizeof(char*) * count);

	if (result)
	{
		size_t idx = 0;
		char* token = strtok(in, delim);
		while (token)
		{
			*(result + idx++) = strdup(token);
			token = strtok(NULL, delim);
		}

		*(result + idx) = 0;
	}

	return result;
}

int str_cut(char* str, int begin, int len)
{
	int l = strlen(str);

	// aby nebyl přistup do nevalidní paměti
	if (len < 0 || begin + len > l)
	{
		len = l - begin;
	}
	memmove(str + begin, str + begin + len, l - len + 1);

	return len;
}
