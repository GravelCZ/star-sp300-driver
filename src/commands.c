/*
Star SP300 Driver for Linux
Copyright (C) 2021  Jindřich Veselý

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "commands.h"

#include <sys/io.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>

uint8_t cmd_buffer[0x10000];
size_t buffer_size;

void send_cmd(uint32_t o_cmd, uint16_t o_param)
{
	uint8_t t_cmd_buf[4];
	int8_t t_cmd_idx = 0;

	#ifdef DEBUG
		fprintf(stderr, "DEBUG: CMD: 0x%08X PARAM: 0x%08X\n", o_cmd, o_param);
	#endif

	while (o_cmd) {
		uint8_t part = o_cmd & 0x000000FF;
		if (part == 0xFF) {
			// FIXME: nefunguje diky tomu ze parameter muze byt 0
			/*if (param == 0x00) {
				printf("ERROR: encountered 0xFF with no replacement in code: 0x%08X param: 0x%08X\n", o_cmd, o_param);
				exit(1);
			}*/
			part = o_param & 0xFF;
			o_param >>= 8;
		}

		t_cmd_buf[t_cmd_idx++] = part;

		#ifdef DEBUG
			fprintf(stderr, "DEBUG: wrote: 0x%08X\n", part);
		#endif

		o_cmd >>= 8;
	}

	if (buffer_size + t_cmd_idx >= sizeof(cmd_buffer))
	{
		printf("ERROR: output buffer overflow!\n");
		exit(1);
	}

	t_cmd_idx--;
	while (t_cmd_idx >= 0)
	{
		cmd_buffer[buffer_size++] = t_cmd_buf[t_cmd_idx--];
	}
}

void send_data(const void* buf, size_t size)
{
	if (buffer_size + size > sizeof(cmd_buffer))
	{
		printf("ERROR: output buffer overflow!\n");
		exit(1);
	}

	if (buf)
	{
		#ifdef DEBUG
			printf("Wrote buffer:");
			size_t i;
			for (i = 0; i < size; i++)
			{
				printf("%02X ", *((uint8_t*)buf + i));
			}
			printf("\n");
		#endif
		memcpy(cmd_buffer + buffer_size, buf, size);
		buffer_size += size;
	}
}

void flush_buf()
{
	#ifndef DRY_RUN
		#ifndef USE_USB
			size_t read_pos = 0;
			while (read_pos < buffer_size) {
				uint8_t byte = cmd_buffer[read_pos++];
				outb(byte, BASE);
				fprintf(stderr, "DEBUG: Sending byte: %02X\n", byte);
				usleep(TRANFER_SLEEP);
			}
		#else
			/*#ifdef DEBUG
				size_t read_pos = 0;
				while (read_pos < buffer_size) {
					fprintf(stderr, "DEBUG: Sending byte: %02X\n", cmd_buffer[read_pos++]);
				}
			#endif*/
			//0x01 v libusb_bulk_transfer je adresa výstupu
			#ifdef DEBUG
				fprintf(stderr, "DEBUG: Buffer size: %zu\n", buffer_size);
			#endif
			// 0x01 - adresa kam zapisovat
			int bytes_written;
			int result = libusb_bulk_transfer(device_handle, (0x01 | LIBUSB_ENDPOINT_OUT), cmd_buffer, buffer_size, &bytes_written, 0);

			#ifdef DEBUG
				fprintf(stderr, "DEBUG: Written %i bytes\n", bytes_written);
			#endif
			if (result < 0) {
				printf("ERROR: Failed to write data to USB: %s\n", libusb_error_name(result));
				exit(1);
			}
		#endif
	#endif

	buffer_size = 0;
}

void clean_buf()
{
	buffer_size = 0;
}
