/*
Star SP300 Driver for Linux
Copyright (C) 2021  Jindřich Veselý

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/
#include "commands.h"
#include "str_utils.h"
#include "image_parser.h"

#include <signal.h>
#include <sys/io.h>
#include <fcntl.h>
#include <string.h>

#define NKEYS_COMMAND (sizeof(command_lookup_table) / sizeof(t_argstruct))
#define NKEYS_FORMAT (sizeof(format_lookup_table) / sizeof(t_cmdstruct))
#define fsend_cmd(x) send_cmd(x, 0)

enum internal_commands {
	BADKEY = -1,
	CMD_BEEP = 1,
	CMD_CUT = 2,
	CMD_CANCEL_AND_INIT = 3,
	CMD_CUSTOM = 4,
	CMD_FLUSH = 5,
	CMD_FEED_PAGE = 6,
	CMD_PRINT_IMAGE = 7,
	CMD_FEED_LINE = 8,
	CMD_PRINT_HELP_TABLE = 9
};

typedef struct {
	char* key;
	int val;
} t_argstruct;

typedef struct {
	char* key;
	uint32_t code;
} t_cmdstruct;

t_argstruct command_lookup_table[] = {
		{ "BEEP", CMD_BEEP },
		{ "CUT", CMD_CUT },
		{ "FEED", CMD_FEED_PAGE },
		{ "LFEED", CMD_FEED_LINE },
		{ "CAI", CMD_CANCEL_AND_INIT },
		{ "CUSTOM", CMD_CUSTOM },
		{ "FLUSH", CMD_FLUSH },
		{ "IMAGE", CMD_PRINT_IMAGE },
		{ "HELPTABLE", CMD_PRINT_HELP_TABLE }
};

t_cmdstruct format_lookup_table[] = {
		{ "EX", PRNT_SELECT_EXPANDED_CHAR_MODE },
		{ "EV", PRNT_SELECT_VERTICAL_EXPADED_CHAR_MODE },
		{ "EP", PRNT_SELECT_EMPHASIZED_PRINT_MODE },
		{ "UD", PRNT_SELECT_UNDERLINE_MODE },
		{ "UP", PRNT_SELECT_UPPERLINE_MODE },
		{ "HL", PRNT_SELECT_HIGHLIGHED_MODE },
		{ "NL", PRNT_LINE_FEED },
		{ "EE", 0 }
};

#ifndef USE_USB
	#ifndef DEBUG
		int perms;
	#endif
#else
struct libusb_device_handle* device_handle = NULL;
libusb_context* ctx = NULL;
#endif

void handle_command(char** command_with_args, size_t n_args);
void handle_parse_text(char* in);
void print_fib();

void handle_command(char** command_with_args, size_t n_args)
{
	#ifdef DEBUG
		fprintf(stderr, "DEBUG: Args: %zu\n", n_args);
	#endif
	size_t i;
	for (i = 0; i < NKEYS_COMMAND; i++) {
		t_argstruct as = command_lookup_table[i];
		if (!strcmp(as.key, *command_with_args))
		{
			switch (as.val) {
				case CMD_BEEP:
					fsend_cmd(PRNT_BUZZER);
					printf("Wrote BEEP command to buffer\n");
					break;
				case CMD_CUT:
					fsend_cmd(PRNT_CUT);
					printf("Wrote CUT command to buffer\n");
					break;
				case CMD_CANCEL_AND_INIT:
					fsend_cmd(PRNT_CANCEL_AND_INIT);
					break;
				case CMD_FEED_PAGE:
					fsend_cmd(PRNT_PAGE_FEED);
					printf("Wrote PAGE FEED command to buffer\n");
					break;
				case CMD_FEED_LINE:
					fsend_cmd(PRNT_LINE_FEED);
					printf("Wrote LINE FEED command to buffer\n");
					break;
				case CMD_CUSTOM:
					if (n_args <= 1)
					{
						printf("No arguments!\n");
						break;
					}
					size_t data_write_pointer = 0;
					size_t data_buffer_size = 20;
					uint8_t* data = malloc(sizeof(char) * data_buffer_size);

					size_t arg_idx;
					for (arg_idx = 1; *(command_with_args + arg_idx); arg_idx++)
					{
						char* num = *(command_with_args + arg_idx);
						if (strlen(num) == 2)
						{
							data[data_write_pointer++] = (uint8_t) strtol(num, NULL, 16);
							if (data_write_pointer == data_buffer_size)
							{
								data_buffer_size += 20;
								data = realloc(data, sizeof(char) * data_buffer_size);
							}
						}
					}

					data = realloc(data, sizeof(char) * data_write_pointer);
					send_data(data, data_write_pointer);
					free(data);
					break;
				case CMD_FLUSH:
					flush_buf();
					printf("Flushed buffer\n");
					break;
				case CMD_PRINT_IMAGE:
					if (n_args <= 1)
					{
						printf("No arguments!\n");
						break;
					}

					char* filename = *(command_with_args + 1);

					printf("Reading: %s\n", filename);

					print_image(filename);

					break;
				case CMD_PRINT_HELP_TABLE:
					handle_parse_text(":EE");
					handle_parse_text("\\EX - :EXExpanded Mode:EE");
					handle_parse_text("\\EV - :EVExpanded Mode:EE");
					handle_parse_text("\\EP - :EPEmphasized Mode:EE");
					handle_parse_text("\\UD - :UDUnderline Mode:EE");
					handle_parse_text("\\UP - :UPUpperline Mode:EE");
					handle_parse_text("\\HL - :HLHighlighted Mode:EE");
					handle_parse_text("\\EE - :EEEnd of any/all modes");
					break;
				case BADKEY:
					break;
			}
		}
	}
}

void handle_parse_text(char* in)
{
	const char* delim = ":";
	char *token, *string_input, *reference;

	reference = string_input = strdup(in);

	while ((token = strsep(&string_input, delim)))
	{
		if (token[0] != 0)
		{
			char* format_code = strdup(token);
			char* actual_text = strdup(token);
			str_cut(format_code, 2, -1); // ustřihne od 2 to dokonce
			str_cut(actual_text, 0, 2); // ustřihne od začátku formatu do dokonce textu

			#ifdef DEBUG
				fprintf(stderr, "Token: '%s', format code: '%s', text part: '%s'\n", token, format_code, actual_text);
			#endif

			int found = 0;
			size_t i;
			for (i=0; i < NKEYS_FORMAT; i++)
			{
				t_cmdstruct cs = format_lookup_table[i];
				if (!strcmp(cs.key, format_code))
				{
					if (cs.code == 0x00)
					{
						fsend_cmd(PRNT_CANCEL_EXPANDED_CHAR_MODE);
						fsend_cmd(PRNT_CANCEL_EMPHASIZED_MODE);
						fsend_cmd(PRNT_CANCEL_HIGHLIGHED_MODE);
						fsend_cmd(PRNT_CANCEL_UNDERLINE_MODE);
						fsend_cmd(PRNT_CANCEL_UPPERLINE_MODE);
						fsend_cmd(PRNT_CANCEL_VERTICAL_EXPADED_CHAR_MODE);
					} else {
						fsend_cmd(cs.code);
					}
					found = 1;
					break;
				}
			}

			if (found)
			{
				//poslat naformatovany text
				send_data(actual_text, strlen(actual_text));
			} else {
				//neni to příkaz takže přidáme zpátky ':' a pošleme do tiskárny
				if (*in == ':') {
					char* correct_message = malloc(strlen(token) + 1);
					correct_message[0] = ':';
					strcpy(correct_message + 1, token);

					send_data(correct_message, strlen(correct_message) - 1);
					free(correct_message);
				} else {
					if (*format_code == '\\') {
						str_cut(token, 0, 1);
					}
					send_data(token, strlen(token));
				}
			}

			free(format_code);
			free(actual_text);
		}
	}

	fsend_cmd(PRNT_LINE_FEED);
	free(reference);
}

static void do_cancel(int s)
{
	(void) s;

	#ifndef DRY_RUN
		#ifndef USE_USB
			fcntl(0, F_SETFL, perms);
			if (ioperm(BASE, 3, 0) == -1)
			{
				printf("ERROR setting permissions on LPT!");
				exit(1);
			}
		#else
			if (device_handle) {
				libusb_release_interface(device_handle, 0);
				libusb_close(device_handle);
				libusb_exit(ctx);
			}
		#endif
	#endif
}

#ifdef USE_USB
int print_device()
{
	libusb_device *device = libusb_get_device(device_handle);

	struct libusb_device_descriptor dev_descr;
	int r = libusb_get_device_descriptor(device, &dev_descr);
	if (r < 0)
	{
		printf("Failed to get device descriptor");
		return 1;
	}

	printf("Number of configurations: %i\n", dev_descr.bNumConfigurations);
	printf("Device class: %i\n", dev_descr.bDeviceClass);

	struct libusb_config_descriptor *config;
	libusb_get_config_descriptor(device, 0, &config);

	printf("Interfaces: %i\n", config->bNumInterfaces);

	struct libusb_interface *interface;
	struct libusb_interface_descriptor *interf_desc;
	struct libusb_endpoint_descriptor *ep_desc;
	for (int i = 0; i < config->bNumInterfaces; i++)
	{
		interface = &config->interface[i];
		printf("Number of settings: %i\n", interface->num_altsetting);

		for (int j = 0; j < interface->num_altsetting; j++)
		{
			interf_desc = &interface->altsetting[j];
			printf("Interface number: %i ", interf_desc->bInterfaceNumber);
			printf("Number of endpoints: %i ", interf_desc->bNumEndpoints);
			for (int k = 0; k < interf_desc->bNumEndpoints; k++)
			{
				ep_desc = &interf_desc->endpoint[k];

				printf(" Descriptor type: %i ", ep_desc->bDescriptorType);
				printf("EP Address: %i", ep_desc->bEndpointAddress);;
			}
			printf("\n");
		}
	}
	return 0;
}
#endif

int main(int argc, char* argv[])
{
	signal(SIGPIPE, SIG_IGN);
	signal(SIGTERM, do_cancel);

	printf("Star SP300 Driver Copyright (C) 2021 Jindřich Veselý\n");
	printf("This program comes with ABSOLUTELY NO WARRANTY\n");
	printf("This is free software, and you are welcome to redistribute it under certain conditions; see LICENSE file\n");

	#ifndef DRY_RUN
		#ifndef USE_USB
			if (ioperm(BASE, 3, 1) == -1) {
				printf("ERROR setting permissions on LPT!\n");
				exit(1);
			}

			perms = fcntl(0, F_GETFL, 0);
			fcntl(0, F_SETFL, (perms | O_NDELAY));
		#else
			int result = libusb_init(&ctx);
			if (result < 0)
			{
				printf("ERROR: Failed to initialize libusb: %s\n", libusb_error_name(result));
				exit(1);
			}

			libusb_set_option(ctx, LIBUSB_OPTION_LOG_LEVEL, LIBUSB_LOG_LEVEL_WARNING);

			device_handle = libusb_open_device_with_vid_pid(ctx, VENDOR_ID, PRODUCT_ID);
			if (device_handle == NULL)
			{
				printf("Failed to find USB device\n");
				libusb_exit(ctx);
				exit(1);
			}

			//Toto je pomocná funkce k vypsání adres adapteru aby se mohlo upravit, muj adapter má jeden interface
			// takto vypadá výpis z meho adapteru
			/*
			Number of configurations: 1
			Device class: 0
			Interfaces: 1
			Number of settings: 3
			Interface number: 0 Number of endpoints: 1  Descriptor type: 5 EP Address: 1
			Interface number: 0 Number of endpoints: 2  Descriptor type: 5 EP Address: 1 Descriptor type: 5 EP Address: 130
			Interface number: 0 Number of endpoints: 3  Descriptor type: 5 EP Address: 1 Descriptor type: 5 EP Address: 130 Descriptor type: 5 EP Address: 131

			Takže já musím otevřít interface číslo 0 a zapisovat na desriptor s adresou 1 
			(musí být typ 5 jelikož to je něco jako IO a ne ostatni blbosti okolo)

			Pokud chcete využít jiný descriptor tak musíte použít libusb_set_interface_alt_setting 
			*/

			/*if (print_device(device_handle))
			{
				exit(1);
			}*/

			if (libusb_kernel_driver_active(device_handle, 0) == 1)
			{
				if (libusb_detach_kernel_driver(device_handle, 0) == 0)
				{
					printf("USB device kernel driver detached!\n");
				}
			}

			//vybere interface 0
			result = libusb_claim_interface(device_handle , 0);
			if (result < 0) {
				printf("Failed to claim interface: %s\n", libusb_error_name(result));
				if (device_handle)
				{
					libusb_close(device_handle);
				}

				libusb_exit(NULL);
				exit(1);
			}
		#endif
	#endif

	fsend_cmd(PRNT_BUZZER);
	send_cmd(PRNT_SET_PAGE_LENGTH_LINES, 25);
	flush_buf();

	printf("READY!\n");
	while (1) {
		char* read = str_read();
		size_t len = strlen(read);
		if (len == 0)
		{
			continue;
		}
		if (*read == '>')
		{
			str_cut(read, 0, 1); // odeber ">"

			if (strlen(read) == 0)
			{
				continue;
			}

			#ifdef DEBUG
				fprintf(stderr, "Parsing command input: %s\n", read);
			#endif

			size_t n_args;
			char** command_with_args = str_split(read, ' ', &n_args); // rozděl podle " "

			if (command_with_args)
			{
				handle_command(command_with_args, n_args);
			}
			//free memory
			int i;
			for(i = 0; *(command_with_args + i); i++)
			{
				free(*(command_with_args + i));
			}
			free(command_with_args);
		} else
		{
			#ifdef DEBUG
				fprintf(stderr, "Would print: %lu: %s\n", len, read);
			#endif
			clean_buf();
			handle_parse_text(read);
			flush_buf();
		}
		free(read);
	}
}

